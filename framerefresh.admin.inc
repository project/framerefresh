<?php

/*
 * Form definition
 */
function framerefresh_admin() {
  $form = array();
  $form['framerefresh_top_frame'] = array(
    '#type' => 'textfield',
    '#title' => t('Top Frame Name'),
	'#default_value' => variable_get('framerefresh_top_frame', ''),
    '#size' => 30,
	'#maxlength' => 30,
    '#description' => t('For example:  playerFrame'),
	'#required' => FALSE
  );
  $form['framerefresh_content_frame'] = array(
    '#type' => 'textfield',
    '#title' => t('Content Frame Name'),
	'#default_value' => variable_get('framerefresh_content_frame', ''),
    '#size' => 30,
	'#maxlength' => 30,
    '#description' => t('For example:  contentFrame'),
	'#required' => FALSE
  );
  
  return system_settings_form($form);
}